package com.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Calculator2Test {

    @Test
    public void testAdd() {
        Calculator2 calculator = new Calculator2();
        assertEquals(5, calculator.add(2, 3), "2 + 3 should equal 5");
    }

}
