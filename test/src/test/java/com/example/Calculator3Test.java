package com.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Calculator3Test {

    @Test
    public void testAdd() {
        Calculator3 calculator = new Calculator3();
        assertEquals(5, calculator.add(2, 3), "2 + 3 should equal 5");
    }

}
