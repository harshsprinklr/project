package com.example;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorMixed {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.subtract(5, 3), "5 - 3 should equal 2");
    }

    @Test
    public void testAdd2() {
        Calculator2 calculator = new Calculator2();
        assertEquals(2, calculator.subtract(5, 3), "5 - 3 should equal 2");
    }

    @Test
    public void testAdd3() {
        Calculator3 calculator = new Calculator3();
        assertEquals(2, calculator.subtract(5, 3), "5 - 3 should equal 2");
    }

}
