import requests
import json
from requests.auth import HTTPBasicAuth
from datetime import datetime
import pytz

# SonarQube server details
sonarqube_url = "http://localhost:9000"
project_key = "multi-module-project2"  # Update this with your actual project key
sonarqube_token="squ_b0614e18d8fd8efe806a6b99fa4b1028106875ea"
page_size = 500
issues = []
auth_sonar = HTTPBasicAuth(sonarqube_token, '')  # Update with your actual credentials

# Jira Configuration (for Jira Cloud)
jira_url = "https://harshsaini5095.atlassian.net"
jira_email = "harshsaini5095@gmail.com"
jira_token = "ATATT3xFfGF02oSdjeBK5n11rh5X9h-YN39JYPyId5uOzZ3sLq2SG6-f8VfF2bRDDjAIK-ULq7rsmKowzefw7hBgT5dkauJH7nVTl2jUak_7gwy9BdaNGbtn2dRs3ODp2ZbYiVLGQ54lxZoPGDVWaUg1H946WOFAzpKtRD5XW-0gL1aqe6vmFrU=CF592793"
jira_project_key = "SON"
auth = HTTPBasicAuth(jira_email, jira_token)

# Create a jira/atlassian format description
def create_adf_description(desc):
    description_adf = {
        "type": "doc",
        "version": 1,
        "content": [
            {
                "type": "paragraph",
                "content": [{"type": "text", "text": desc}]
            }
        ]
    }
    return description_adf

# Get current analysis date and times
def get_analyses(project_key, page_size=100):
    analyses = []
    page = 1

    while True:
        # Construct the API URL with pagination
        url = f"{sonarqube_url}/api/project_analyses/search"
        params = {
            'project': project_key,
            'page': page,
            'pageSize': page_size
        }

        # Make the request
        response = requests.get(url, params=params, auth=(sonarqube_token, ''))
        if response.status_code != 200:
            raise Exception(f"Error fetching analyses: {response.status_code} {response.text}")

        data = response.json()
        analyses.extend(data['analyses'])

        # Check if we need to fetch another page
        if page * page_size >= data['paging']['total']:
            break

        page += 1

    return analyses
analyses = get_analyses(project_key)
analysis_dates = [analysis['date'] for analysis in analyses]
cutoff_date = datetime.strptime("2023-06-25T00:00:00+0000", "%Y-%m-%dT%H:%M:%S%z")
cutoff_date = analysis_dates[0]
print(cutoff_date)

# Function to add comment to a Jira issue
def add_comment_to_issue(issue_key, comment):
    url = f"{jira_url}/rest/api/3/issue/{issue_key}/comment"
    data = {
        "body": comment
    }

    response = requests.post(url, json=data, headers={"Content-Type": "application/json"}, auth=auth)
    
    if response.status_code == 201:
        print(f"Successfully added comment to issue {issue_key}: {comment}")
    else:
        print(f"Failed to add comment to issue {issue_key}: {response.text}")

# Function to fetch all issues in a Jira project and add comment to older issues
def fetch_all_issues_and_comment(project_key):
    comment = create_adf_description(f"Issue persists in {cutoff_date}")
    start_at = 0
    max_results = 100  # Adjust based on your needs
    total = 1  # Initial dummy value to enter the loop

    while start_at < total:
        url = f"{jira_url}/rest/api/3/search"
        params = {
            "jql": f"project={project_key}",
            "startAt": start_at,
            "maxResults": max_results
        }
        headers = {
            "Accept": "application/json"
        }

        response = requests.get(url, params=params, headers=headers, auth=auth)

        if response.status_code == 200:
            data = response.json()
            issues = data.get("issues", [])
            total = data.get("total", 0)

            for issue in issues:
                issue_key = issue["key"]
                issue_summary = issue["fields"]["summary"]
                issue_created_str = issue["fields"]["created"]

                # Add comment to the issue
                add_comment_to_issue(issue_key, comment)

                # Print issue details
                print(f"Issue Key: {issue_key}, Summary: {issue_summary}, Created: {issue_created_str}")

            start_at += len(issues)
        else:
            print(f"Failed to fetch issues: {response.status_code}, {response.text}")
            break

# Fetch all issues in the specified Jira project and add comments to older issues
fetch_all_issues_and_comment(jira_project_key)