import requests
import json
from requests.auth import HTTPBasicAuth
import pytz
from datetime import datetime

# SonarQube server details
sonarqube_url = "http://localhost:9000"
project_key = "multi-module-project2"  # Update this with your actual project key
sonarqube_token="squ_b0614e18d8fd8efe806a6b99fa4b1028106875ea"
page_size = 500
issues = []
auth_sonar = HTTPBasicAuth(sonarqube_token, '')  # Update with your actual credentials

# Jira Configuration (for Jira Cloud)
jira_url = "https://harshsaini5095.atlassian.net"
jira_email = "harshsaini5095@gmail.com"
jira_token = "ATATT3xFfGF02oSdjeBK5n11rh5X9h-YN39JYPyId5uOzZ3sLq2SG6-f8VfF2bRDDjAIK-ULq7rsmKowzefw7hBgT5dkauJH7nVTl2jUak_7gwy9BdaNGbtn2dRs3ODp2ZbYiVLGQ54lxZoPGDVWaUg1H946WOFAzpKtRD5XW-0gL1aqe6vmFrU=CF592793"
jira_project_key = "SON"
auth_jira = HTTPBasicAuth(jira_email, jira_token)

# Get past analysis date and times
def get_analyses(project_key, page_size=100):
    analyses = []
    page = 1

    while True:
        # Construct the API URL with pagination
        url = f"{sonarqube_url}/api/project_analyses/search"
        params = {
            'project': project_key,
            'page': page,
            'pageSize': page_size
        }

        # Make the request
        response = requests.get(url, params=params, auth=(sonarqube_token, ''))
        if response.status_code != 200:
            raise Exception(f"Error fetching analyses: {response.status_code} {response.text}")

        data = response.json()
        analyses.extend(data['analyses'])

        # Check if we need to fetch another page
        if page * page_size >= data['paging']['total']:
            break

        page += 1

    return analyses
analyses = get_analyses(project_key)
analysis_dates = [analysis['date'] for analysis in analyses]
cutoff_date = datetime.strptime("2023-06-25T00:00:00+0000", "%Y-%m-%dT%H:%M:%S%z")
if(len(analysis_dates)) >= 2:
    cutoff_date = analysis_dates[1]
print(cutoff_date)

# Create a jira/atlassian format description
def create_adf_description(desc):
    description_adf = {
        "type": "doc",
        "version": 1,
        "content": [
            {
                "type": "paragraph",
                "content": [{"type": "text", "text": desc}]
            }
        ]
    }
    return description_adf

# Find the user's account ID by email
def get_user_id_by_email(email):
    url = f"{jira_url}/rest/api/3/user/search"
    query = {
        'query': email
    }
    headers = {
        'Accept': 'application/json'
    }

    response = requests.get(url, headers=headers, params=query, auth=auth_jira)

    if response.status_code == 200:
        users = response.json()
        if users:
#             print(users)
            user_id = users[0]['accountId']
            return user_id
        else:
            print(email)
            print("No user found with that email.")
    else:
        print(f"Failed to fetch user: {response.status_code}")
        print(response.text)
    return None

# Fetch issues from SonarQube from latest to old
def fetch_sonar_issues():
    page_number = 1
    while True:
        params = {
            'components': project_key,
            'sort': 'CREATION_DATE',
            'asc': 'false',  # false for descending order
            'ps': page_size,
            'p': page_number,
            "resolved": "false"
        }
        
        response = requests.get(f"{sonarqube_url}/api/issues/search", params=params, auth=auth_sonar)
        
        try:
            data = response.json()
        except ValueError:
            print("Error: Unable to decode JSON response")
            print("Response Text:", response.text)
            break
        
        if 'issues' in data:
            issues.extend(data['issues'])
            if len(data['issues']) < page_size:
                break

            page_number += 1
        else:
            print("Error: No issues found in the response")
            break

    print(f"Total issues retrieved: {len(issues)}")
fetch_sonar_issues()

# Create issues in Jira and assign to the specified user which are created after the last analysis date
def create_jira_ticket():
    for issue in issues:
        issue_id = issue["key"]
        issue_author = issue["author"]
        issue_date = issue["creationDate"]
        sonarqube_issue_url = f"{sonarqube_url}/project/issues?id={project_key}&open={issue_id}"

        if(issue_date < cutoff_date):
            break

        # if(issue_author == "harsh.saini@sprinklr.com"):
        #     issue_author = "harshsaini5095@gmail.com"

        jira_assignee = get_user_id_by_email(issue_author)
        jira_issue = {
            "fields": {
                "project": {
                    "key": jira_project_key
                },
                "summary": issue["message"],
                "description": create_adf_description(f"Link : {sonarqube_issue_url} SonarQube issue: {issue['key']}\n\n{issue['message']}"),
                "issuetype": {
                    "name": "Bug"
                },
                "assignee": {
                    "accountId": jira_assignee  # Assign to the specified user by account ID
                },
                'labels': [issue_id]
            }
        }
        
        response = requests.post(
            f"{jira_url}/rest/api/3/issue",
            headers={"Content-Type": "application/json"},
            data=json.dumps(jira_issue),
            auth=(jira_email, jira_token)
        )
        
        if response.status_code == 201:
            print(f"Successfully created Jira issue for SonarQube issue {issue['key']} and assigned to {jira_assignee}")
        else:
            print(f"Failed to create Jira issue for SonarQube issue {issue['key']}: {response.text}")
create_jira_ticket()